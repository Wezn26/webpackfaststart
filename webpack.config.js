//BASIC VARIABLES
const path = require('path');
const webpack = require('webpack');

//ADDITIONAL PLUGINS
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJSplugin = require('uglifyjs-webpack-plugin');
const CopyWebpackplugin = require('copy-webpack-plugin');
const ImageMinPlugin = require('imagemin-webpack-plugin');

let isProduct = process.env.NODE_ENV === 'production';

//MODULE SETTINGS
module.exports = {
  mode: "development",
  //базовый путь к проекту
   context: path.resolve(__dirname, 'src'),

   //токи входа js
   entry: {
     //основной файл приложений
     app: [
       './js/app.js',
       './scss/style.scss'
       ],
   },
   //путь для собранных файлов
   output: {
     filename: 'js/[name].js',
     path: path.resolve(__dirname, 'dist'),
     publicPath: '../'
   },
   //dev server configuration
   devServer: {
     contentBase: './app'
   },

   devtool: (isProduct) ? '' : 'inline-source-map',



   module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {loader:'css-loader', options: { sorceMap: true}},
            {loader:'postcss-loader', options: { sorceMap: true}},
            {loader:'sass-loader',  options: { sorceMap: true}}
            // 'style-loader'
        ]

      },
      //Image
      {
        test: /\.(png|gif|jpe?g)$/,
        loaders: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',

            },
          },
          'img-loader'
        ]
      },
      ]
    },

   plugins: [
       new MiniCssExtractPlugin({
          publicPath: './css',
          filename: '[name].css'

       })
     ]

};
